---
layout: post
title: Sekilas Tentang Linux Class Malang
date: '2018-12-01'
author: Samsul Maarif
categories: blog
tags:
- Workshop
- CentOS
- DOT
- Linux
cover-img: 
  - "/img/peserta-linux-class-malang-1.jpg" : "Peserta Linux Class Malang, (2018)"
share-img: "/img/peserta-linux-class-malang-1.jpg"
---

Beberapa waktu lalu saya membuat sebuah workshop di kantor di mana saya bekerja saat ini. Kelas tersebut bertajuk **Linux Class Malang**, merupakan kelas kecil dengan peserta 10 orang. Materi yang dibahas dan lain-lain dapat teman-teman lihat di poster berikut :

|![](/img/linux-class-malang-1.jpg)|
|**Poster Linux Class Malang**|

Awalnya agak ragu ketika membuka kelas ini. Dengan target peserta dari kalangan Guru SMK (utamanya TKJ), harapannya akan lebih bermanfaat bagi sekolah. Namun, pada kenyataannya tidak semua calon peserta dari kalangan guru memiliki waktu luang pada tanggal tersebut. Menjelang hari penutupan pendaftaran kursi yang tersedia masih cukup banyak. Akhirnya kita buka kursi tersebut untuk siapa saja yang berminat untuk ikut. 

Workshop tersebut berlangsung dua hari, Sabtu-Minggu, 27-28 Oktober 2018 berlangsung di DOT Techno Valley. Yap, itu semua ada di poster. 

|![](http://i.imgur.com/9yB4y7E.png)|
|**Koleksi foto acara**|

Untuk tahu lebih banyak tentang acara tersebut, teman-teman bisa baca postingan peserta di daftar ini :

1. [Sidarmawan](https://sidarmawan8.blogspot.com/2018/11/linux-class-malang.html)
2. [Subkhan Akbar](https://inspirasi23.wordpress.com/2018/11/05/training-sysadmin/)
3. [Leonardus Kristaris Sastra](https://leon036.blogspot.com/2018/11/feedback-linux-sysadmin-class-malang.html)
4. [Muhammad Khifni](https://keepedutechlife.wordpress.com/2018/11/05/kelas-linux-dot-indonesia/)
5. [Satria Adhi Kharisma](https://medium.com/@sakharisma/my-training-experience-in-linux-system-administration-class-malang-3b4ed10e5660)
6. [Rifqi Rosyidi](https://medium.com/@rief.rosyidi/belajar-linux-dan-sysadmin-dalam-2-hari-d-7a9b2fbf9cad)
7. [Arief Wahyudin](https://www.facebook.com/smk.alhikmah.jbg/posts/1934254296869477)
8. [Henry Cahyo Nugroho](https://digitechpencil.blogspot.com/2018/11/pelatihan-sysadmin.html)
9. [Ragata Anggada Bhakti](https://ragataab.blogspot.com/2018/11/asalamualaikum-wr.html)
10. [Fatkul Toriq](https://fatoriq.blogspot.com/2018/11/belajar-kilat-pernah-gak-melakukan.html)

Nah, sudah baca beberapa? oh belum? coba aja klik salah satu namanya... 

|![](https://cdn-images-1.medium.com/max/1200/1*OnJvqL9QN27m79kNqIpU6A.jpeg)|
|**Bersama para peserta di hari terakhir**|

Oiya, seperti yang sudah di tulis di postingan para perserta di link di atas, acara ini terselenggara berkat dukungan sponsor [CloudKilat](https://www.cloudkilat.com) yang telah menyediakan VPS untuk praktek peserta, [DOT Indonesia](https://www.dot.co.id) yang telah menyediakan tempat dan segala periferalnya dan partner kami [Indiekraf](http://indiekraf.com), [KLiM](https://www.klim.or.id).

|![](/img/peserta-linux-class-malang-1-sertifikat.jpg)|
|**Peserta dapat sertifikat lho...**|


Dah, sekian dulu catatannya ini tak sudahi. Masih belum mood untuk menulis panjang-panjang. Mungkin karena sudah terlalu lama tidak ngeblog. Tunggu kelas kami selanjutnya ya... Rencananya sih untuk kelanjutannya mau bikin kelas [Docker](/2017/06/berkenalan-dengan-docker.html) :-D 