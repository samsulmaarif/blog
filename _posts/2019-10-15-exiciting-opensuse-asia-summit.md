---
layout: post
title: Exiciting openSUSE Asia Summit 2019, Bali
date: '2019-10-15'
author: Samsul Maarif
categories: blog
tags:
- oSAS19
- Linux
- openSUSE
cover-img:
  - "/img/osas/osas-0.jpg" : "openSUSE Asia Summit, Bali (2019)"
share-img: "/img/osas/osas-0.jpg"
---


So, this is the first time i attend openSUSE Asia Summit. This year openSUSE Asia Summit 2019 has been held in Udayana University, 5-6 October 2019.

Why i attend this event? I was skeptical when i submit my paper the [CFP is open](https://news.opensuse.org/2019/05/01/opensuse-asia-summit-2019-bali-call-for-proposals-is-open/) in which i only submit one paper. Will my paper be accepted? The submission is pretty straight forward though, just type events.opensuse.org, register there, complete my profile, choose the event and then i submit my title **Deploy Multinode GitLab Runner in openSUSE 15.1 Instances with Ansible Automation**. I was planning to submit another title, but i couldn't find one appropriate until the deadline.

I was verry happy when i receive the acceptance email from the CFP team of the openSUSE Asia Summit 2019 committee. I myself has made a commitment that i wrote in my facebook page something like "I will switch to openSUSE if my paper accepted." A month before the event i manage to replace my Ubuntu Studio 16.04 with openSUSE Leap 15.1. Yes, long time being an Ubuntu user (since 2013) now switching to openSUSE.

### Pre-event

After long journey from Malang to Bali, more than 16 hours, i finally arrive in Bali at 11:00 WITA (Bali time), 4 October 2019. I went to Bali with a group of KLAS, KLiM, and Doscom. We even made [a banner](https://github.com/hervyqa/artwork-documentation/blob/master/klasxklim/banner-2x1m.png) for our picnic journey.

![Before we go](/img/osas/osas-1.jpg)

I decided to take a bath in Buffallo surf where some of our stay before move to my hotel which the check-in time was 15:00 WITA.


In the evening i went to Wellcome Dinner location on New Furama Cafe, Jimbaran Beach. The view is very wonderful. I arrive early with my friend Imanuel, Andhika, and pak Sabar. We met Johannes Segitz, a security engineer from SuSE and had a short talk while walking ont the beach.

* ![Meet Johannes Segitz](/img/osas/osas-2.jpg)
* ![Ish Shookun interviewed by Imanuel](/img/osas/osas-3.jpg)
* ![Waiting for sunset](/img/osas/osas-4.jpg)

When other speaker and sponsor arrive, my friend Imanuel take the opportunity to make an interview with Ish Shookun. Imanuel is a content writer at [kabarlinux.id](https://kabarlinux.id), he wrote anything about open source and Linux.

### Day one, Saturday

I went to venue from hotel with Go-Ride after member of KLASxKLiM telegram group tell that the traffic is so high. At the venue, registration for speaker was at the 4th floor, and there was no lift. :D

A ceremonial dance, Tari Sekar Jagad was performed when opening the Summit. And after that keynote speaker, Simon Lee and Dr. Axel Braun from the openSUSE Board presenting about status update about the openSUSE project. He also introduced Gerald Pfeifer as the new Chairperson of openSUSE.

After coffee break, i join Sabar Suwarsono's class. He talk about [**Data Science Environment with R on openSUSE Leap 15.1**](https://events.opensuse.org/conferences/summitasia19/program/proposals/2591), he explain about data science and using of R for analyzing social media trend in the demo.

My class was scheduled after lunch, i talked about GitLab, it's runner, and how to deploy multiple instances with Ansible script to automate the process. After a quick demo and Q&A session, i gave a souvenir for the best question from attendee.

* ![The souvenir](/img/osas/osas-7.jpg)
* ![Best question](/img/osas/osas-5.jpg)
* ![yay, it's me in action](/img/osas/osas-6.jpg)

She ask about how QA team collaborate with other team by implementing CI/CD in their project.

### Day two, Sunday

...
