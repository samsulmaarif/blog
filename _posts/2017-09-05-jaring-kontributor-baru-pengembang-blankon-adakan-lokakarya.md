---
layout: post
title: Jaring Kontributor Baru, Pengembang BlankOn Adakan Lokakarya
date: '2017-09-05'
author: Samsul Maarif
categories: blog
tags:
  - SysAdmin
  - Lokakarya
  - Workshop
  - Linux
  - Surabaya
cover-img: 
  - "/img/lokakarya-blankon-surabaya-2.jpg" : "Lokakarya SysAdmin Blankon, Surabaya (2017)"
share-img: "/img/lokakarya-blankon-surabaya-2.jpg"
---

Sabtu, 2 September 2017 lalu, pengembang blankon, khususnya tim Infrastruktur mengadakan “Lokakarya Adminsistrasi Sistem Linux untuk pemula.”

Lokakarya ini diselenggarakan di ruang meeting PT. Rahajasa Media Internet (Radnet), di Plaza BRI Tower suite 803, Jl. Basuki Rahmat No.122, Embong Kaliasin, Genteng, Kota Surabaya, Jawa Timur.

Dunia perangkat lunak bebas dan terbuka tak akan terlepas dari para kontributor yang memiliki komitmen tinggi dan sukarela membaktikan waktu, tenaga dan pikirannya.

Tak terkecuali BlankOn Linux, sebuah distro Linux cita rasa Indonesia yang dikembangkan oleh Yayasan Penggerak Linux Indonesia bersama Pengembang BlankOn.
Jaring Kontributor Baru

“Saat ini, dan di masa yang akan datang, BlankOn Linux membutuhkan banyak kontributor yang konsisten menjadi pengembang,” ungkap Darian Rizaludin, koordinator tim infrastruktur Pengembang BlankOn.

“Acara ini bertujuan selain untuk menjaring kontributor baru, adalah untuk kaderisasi sysadmin,” lanjutnya.

Menurut mahasiswa jomblo yang sedang nangkring di semester lima ini, pesertanya memang dibatasi karena kapasitas ruang meeting yang terbatas, hanya 10 orang. “Pesertanya saya seleksi,” pungkasnya.

|![](/img/lokakarya-blankon-surabaya-1-1024x768.jpg)|
|**Peserta Lokakarya Administrator Sistem Linux di Radnet Surabaya (foto/istimewa)**|

Hadir sebagai pemateri, Samsul Ma’arif, IT SysAdmin PuskoMedia Indonesia. Materinya antara lain proses booting, manajemen user, manajemen hak akses file dan direktori, manajemen paket, DNS Server, FTP Server, Web Server, dll.

“Antusiasme peserta cukup tinggi, dan saya lihat pengguna Linux di Surabaya ini cukup heterogen, dari berbagai kalangan. Ada pelajar SMK, Guru, Mahasiswa, dan pegawai kantoran pun menggunakan Linux,” ungkapnya.

“Harapannya semoga ada lagi acara seperti ini, karena bermanfaat sekali bagi saya,” ujar Funda, salah seorang peserta dari Malang.

> Catatan: Copas dari tulisan lama di blog sebelah, tapi tulisan saya sendiri lho....