---
layout: post
title: Catatan Penerjemahan LibreOffice 5-6 Januari 2019 @ DOT Malang
date: '2019-01-19'
author: Samsul Maarif
categories: blog
tags:
- BlankOn
- Blankoding
- Malang
- Linux
cover-img: 
  - "/img/peserta-linux-class-malang-1.jpg" : "Kampung 3D Jodipan, Malang (2018)"
share-img: "/img/peserta-linux-class-malang-1.jpg"
---

Pada saat tulisan ini diterbitkan, telah berlangsung acara penerjemahan di Surabaya. Jadi sangat telat saya postingnya. :-D :-D :-D 

Menyusul kegiatan Penerjemahan LibreOffice di dua kota sebelumnya, yaitu di Gresik (pada 3-4 November 2018) dan Yogyakarta (pada 1-2 Desember 2018), kali ini giliran kota Malang yang menyelenggarakan acara translation hackathon tersebut. 

Komunitas GNU/Linux Malang (KLiM) bekerja sama dengan [Kelompok Linux Arek Surabaya (KLAS)](http://klas.or.id) dengan dukungan DOT Indonesia, serta disponsori oleh pabrik [Sepatu Fans](http://www.fans.co.id/) menyelenggarakan Penerjemahan LibreOffice ke Bahasa Indonesia. Acara yang diselenggarakan di [DOT Techno Valley](http://www.dot.co.id), Perum Permata Hijau A.15, Tlogomas, Malang ini berlangsung seru. 

Selama 2 hari, yaitu pada Sabtu s.d. Minggu, 5-6 Januari 2019 peserta yang datang dari beberapa kota antusias mengikuti acara yang bertujuan untuk berkontribusi pada proyek sumber terbuka ini. Beberapa peserta datang dari Jember, Pasuruan, Gresik, serta Malang.

Target acara ini adalah untuk menerjemahkan Master Help